/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.              Rebane
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include "socket_tcp.h"
#include "rtsp_proxy.h"

int main(int argc, char *argv[]){
	char accept_address[16];
	int client_address[4];
	int listen_socket, client_socket, pid;

	if(argc < 6){
		fprintf(stderr, "usage\n");
		return(1);
	}

	signal(SIGCHLD, SIG_IGN);

	listen_socket = socket_tcp_listen(argv[1], argv[2], 5);
	if(listen_socket < 0){
		fprintf(stderr, "socket: %s\n", strerror(errno));
		return(1);
	}

	while(1){
		client_socket = socket_tcp_accept(listen_socket, accept_address, NULL);
		if(client_socket < 0){
			fprintf(stderr, "accept: %s\n", strerror(errno));
		}else{
			pid = fork();
			if(pid < 0){
				fprintf(stderr, "fork: %s\n", strerror(errno));
			}else if(pid > 0){
				// printf("accepted...\n");
				close(client_socket);
			}else{
				sscanf(accept_address, "%d.%d.%d.%d", &client_address[0], &client_address[1], &client_address[2], &client_address[3]);
				printf("MAIN: CLIENT: %d.%d.%d.%d\n", client_address[0], client_address[1], client_address[2], client_address[3]);
				rtsp_proxy(client_socket, client_address, argv[3], argv[4], strtoull(argv[5], NULL, 0));
				socket_tcp_close(client_socket);
				printf("MAIN: RTSP PROXY CLEANUP\n");
				exit(1);
			}
		}
	}
}

