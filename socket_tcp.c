/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.              Rebane
 * ----------------------------------------------------------------------------
 */

#include "socket_tcp.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <linux/sockios.h>

int socket_tcp_listen(char *listen_address, char *listen_port, int backlog){
	struct sockaddr_in addr;
	int sock, t;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0)return(sock);

	t = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	if(listen_address != NULL){
		addr.sin_addr.s_addr = inet_addr(listen_address);
	}else{
		addr.sin_addr.s_addr = INADDR_ANY;
	}
	addr.sin_port = htons(atoi(listen_port));
	t = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if(t < 0){
		socket_tcp_close(sock);
		return(t);
	}

	t = listen(sock, backlog);
	if(t < 0){
		socket_tcp_close(sock);
		return(t);
	}
	return(sock);
}

int socket_tcp_accept(int listen_socket, char *accept_address, char *accept_port){
	struct sockaddr addr;
	int sock;
	socklen_t sl;
	sl = sizeof(addr);
	sock = accept(listen_socket, &addr, &sl);
	if(sock < 0)return(sock);
	if(accept_address != NULL){
		sprintf(accept_address, "%u.%u.%u.%u", (unsigned int)((uint8_t)addr.sa_data[2]), (unsigned int)((uint8_t)addr.sa_data[3]), (unsigned int)((uint8_t)addr.sa_data[4]), (unsigned int)((uint8_t)addr.sa_data[5]));
	}
	if(accept_port != NULL){
		sprintf(accept_port, "%u", (unsigned int)(((uint16_t)((uint8_t)addr.sa_data[0]) << 8) | (uint8_t)addr.sa_data[1]));
	}
	return(sock);
}

int socket_tcp_connect(char *bind_address, char *bind_port, char *connect_address, char *connect_port){
	struct addrinfo hints, *result;
	struct sockaddr_in addr;
	int sock, t;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if(getaddrinfo(connect_address, connect_port, &hints, &result))return(-1);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0){
		freeaddrinfo(result);
		return(sock);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	if(bind_address != NULL){
		addr.sin_addr.s_addr = inet_addr(bind_address);
	}else{
		addr.sin_addr.s_addr = INADDR_ANY;
	}
	if(bind_port != NULL){
		addr.sin_port = htons(atoi(bind_port));
	}else{
		addr.sin_port = htons(0);
	}
	t = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if(t < 0){
		socket_tcp_close(sock);
		freeaddrinfo(result);
		return(t);
	}

	t = connect(sock, result->ai_addr, result->ai_addrlen);
	if(t < 0){
		socket_tcp_close(sock);
		freeaddrinfo(result);
		return(t);
	}

	freeaddrinfo(result);
	return(sock);
}

void socket_tcp_close(int socket){
	shutdown(socket, SHUT_RDWR);
	close(socket);
}

int socket_tcp_send_len(int socket){
	int s, p, t;
	socklen_t sl;
	sl = sizeof(s);
	t = getsockopt(socket, SOL_SOCKET, SO_SNDBUF, &s, &sl);
	if(t < 0)return(t);
	t = ioctl(socket, SIOCOUTQ, &p);
	if(t < 0)return(t);
	if(p > s)return(-1);
	return(s - p);
}

