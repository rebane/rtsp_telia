/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.              Rebane
 * ----------------------------------------------------------------------------
 */

#ifndef _SOCKET_TCP_H_
#define _SOCKET_TCP_H_

int socket_tcp_listen(char *listen_address, char *listen_port, int backlog);
int socket_tcp_accept(int listen_socket, char *accept_address, char *accept_port);
int socket_tcp_connect(char *bind_address, char *bind_port, char *connect_address, char *connect_port);
void socket_tcp_close(int socket);
int socket_tcp_send_len(int socket);

#endif

