/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.              Rebane
 * ----------------------------------------------------------------------------
 */

#ifndef _RTSP_PROXY_H_
#define _RTSP_PROXY_H_

void rtsp_proxy(int client_socket, int *client_address, char *default_server_address, char *default_server_port, unsigned long int timeout);

#endif

