/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <rebane@alkohol.ee> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.              Rebane
 * ----------------------------------------------------------------------------
 */

#include "rtsp_proxy.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <errno.h>
#include "socket_tcp.h"

#define RTSP_PROXY_LINE_MAX    4096
#define RTSP_PROXY_BUFFER_MAX  1024
#define RTSP_PROXY_CLEANUP_MAX 1024

static int rtsp_proxy_server_socket;
static char rtsp_proxy_cleanup_command[1024];
static int rtsp_proxy_client_parse(char *client_line, int client_loc, char *default_server_address, char *default_server_port);
static void rtsp_proxy_server_parse(char *server_line, int server_loc, int *client_address);
static void rtsp_proxy_cleanup();
static unsigned long int rtsp_proxy_system_uptime();

void rtsp_proxy(int client_socket, int *client_address, char *default_server_address, char *default_server_port, unsigned long int timeout){
	char client_line[RTSP_PROXY_LINE_MAX];
	char server_line[RTSP_PROXY_LINE_MAX];
	int client_loc, server_loc;
	char buffer[RTSP_PROXY_BUFFER_MAX];
	struct timeval tv;
	fd_set fds;
	size_t l;
	int i, s;
	unsigned long int timer;

	rtsp_proxy_server_socket = -1;
	rtsp_proxy_cleanup_command[0] = 0;
	client_loc = server_loc = 0;
	timer = rtsp_proxy_system_uptime() + timeout;

	while(1){
		FD_ZERO(&fds);
		FD_SET(client_socket, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 50000;
		if(rtsp_proxy_server_socket < 0){
			s = select(client_socket + 1, &fds, NULL, NULL, &tv);
		}else{
			FD_SET(rtsp_proxy_server_socket, &fds);
			s = select(client_socket > rtsp_proxy_server_socket ? (client_socket + 1) : (rtsp_proxy_server_socket + 1), &fds, NULL, NULL, &tv);
		}
		if(s > 0){
			if(FD_ISSET(client_socket, &fds)){
				l = read(client_socket, buffer, RTSP_PROXY_BUFFER_MAX);
				if(l < 0){
					fprintf(stderr, "read: %s\n", strerror(errno));
					rtsp_proxy_cleanup();
					return;
				}else if(l > 0){
					timer = rtsp_proxy_system_uptime() + timeout;
					for(i = 0; i < l; i++){
						client_line[client_loc] = buffer[i];
						if(buffer[i] == '\n'){
							client_line[++client_loc] = 0;
							client_loc = rtsp_proxy_client_parse(client_line, client_loc, default_server_address, default_server_port);
							if(rtsp_proxy_server_socket < 0){
								rtsp_proxy_cleanup();
								return;
							}
							write(rtsp_proxy_server_socket, client_line, client_loc);
							client_loc = 0;
						}else{
							if(client_loc < (RTSP_PROXY_LINE_MAX - 2))client_loc++;
						}
					}
				}else{
					rtsp_proxy_cleanup();
					return;
				}
			}
			if((rtsp_proxy_server_socket >= 0) && FD_ISSET(rtsp_proxy_server_socket, &fds)){
				l = read(rtsp_proxy_server_socket, buffer, RTSP_PROXY_BUFFER_MAX);
				if(l < 0){
					fprintf(stderr, "read: %s\n", strerror(errno));
					rtsp_proxy_cleanup();
					return;
				}else if(l > 0){
					timer = rtsp_proxy_system_uptime() + timeout;
					for(i = 0; i < l; i++){
						server_line[server_loc] = buffer[i];
						if(buffer[i] == '\n'){
							server_line[++server_loc] = 0;
							rtsp_proxy_server_parse(server_line, server_loc, client_address);
							write(client_socket, server_line, server_loc);
							server_loc = 0;
						}else{
							if(server_loc < (RTSP_PROXY_LINE_MAX - 2))server_loc++;
						}
					}
				}else{
					rtsp_proxy_cleanup();
					return;
				}
			}
		}else if(s < 0){
			fprintf(stderr, "select: %s\n", strerror(errno));
			rtsp_proxy_cleanup();
			return;
		}
		if(rtsp_proxy_system_uptime() > timer){
			fprintf(stderr, "timeout\n");
			rtsp_proxy_cleanup();
			return;
		}
	}
}

static int rtsp_proxy_client_parse(char *client_line, int client_loc, char *default_server_address, char *default_server_port){
	char *d;
	int i, l, server_address_int[4];
	unsigned int server_port_int;
	char server_address[16], server_port[8];
	printf("RTSP PROXY: CLIENT: %s", client_line);
	if(rtsp_proxy_server_socket < 0){
		do{
			d = strstr(client_line, "rtsp://");
			if(d == NULL)break;
			if(sscanf(&d[7], "%d.%d.%d.%d:%u", &server_address_int[0], &server_address_int[1], &server_address_int[2], &server_address_int[3], &server_port_int) == 5){
				snprintf(server_address, 16, "%d.%d.%d.%d", server_address_int[0], server_address_int[1], server_address_int[2], server_address_int[3]);
				snprintf(server_port, 8, "%u", server_port_int);
				default_server_address = server_address;
				default_server_port = server_port;
				printf("RTSP PROXY: PARSED SERVER: %s:%s\n", server_address, server_port);
			}else if(sscanf(&d[7], "%d.%d.%d.%d", &server_address_int[0], &server_address_int[1], &server_address_int[2], &server_address_int[3]) == 4){
				snprintf(server_address, 16, "%d.%d.%d.%d", server_address_int[0], server_address_int[1], server_address_int[2], server_address_int[3]);
				default_server_address = server_address;
				printf("RTSP PROXY: PARSED SERVER: %s\n", server_address);
			}else{
				break;
			}
		}while(0);
		printf("RTSP PROXY: USING SERVER: %s:%s\n", default_server_address, default_server_port);
		rtsp_proxy_server_socket = socket_tcp_connect(NULL, NULL, default_server_address, default_server_port);
		return(client_loc);
	}else{
		d = strstr(client_line, "destination=");
		if(d == NULL)return(client_loc);
		l = d - client_line;
		printf("RTSP PROXY: MATCHED DESTINATION: %s", &client_line[l]);
		for(i = l; client_line[i]; i++){
			if(client_line[i] == ';'){
				i++;
				break;
			}
			if(client_line[i] == '\r')break;
			if(client_line[i] == '\n')break;
		}
		memmove(&client_line[l], &client_line[i], client_loc - i + 1);
		client_loc -= (i - l);
		printf("RTSP PROXY: CLIENT NEW: %s", client_line);
		return(client_loc);
	}
}

static void rtsp_proxy_server_parse(char *server_line, int server_loc, int *client_address){
	char *d;
	int destination_address[4], source_address[4];
	int destination_port, source_port;

	printf("RTSP PROXY: SERVER: %s", server_line);
	if(strncmp(server_line, "Transport:", 10))return;
	printf("RTSP PROXY: MATCHED TRANSPORT\n");

	d = strstr(server_line, "destination=");
	if(d == NULL)return;
	if(sscanf(d + 12, "%d.%d.%d.%d", &destination_address[0], &destination_address[1], &destination_address[2], &destination_address[3]) != 4)return;
	printf("RTSP PROXY: DESTINATION: %d.%d.%d.%d\n", destination_address[0], destination_address[1], destination_address[2], destination_address[3]);

	d = strstr(server_line, "source=");
	if(d == NULL)return;
	if(sscanf(d + 7, "%d.%d.%d.%d", &source_address[0], &source_address[1], &source_address[2], &source_address[3]) != 4)return;
	printf("RTSP PROXY: SOURCE: %d.%d.%d.%d\n", source_address[0], source_address[1], source_address[2], source_address[3]);

	d = strstr(server_line, "client_port=");
	if(d == NULL)return;
	if(sscanf(d + 12, "%d", &destination_port) != 1)return;
	printf("RTSP PROXY: DESTINATION PORT: %d\n", destination_port);

	d = strstr(server_line, "server_port=");
	if(d == NULL)return;
	if(sscanf(d + 12, "%d", &source_port) != 1)return;
	printf("RTSP PROXY: SOURCE PORT: %d\n", source_port);

	if(rtsp_proxy_cleanup_command[0] != 0){
		printf("RTSP PROXY: COMMAND: %s\n", rtsp_proxy_cleanup_command);
		system(rtsp_proxy_cleanup_command);
		rtsp_proxy_cleanup_command[0] = 0;
	}
	snprintf(rtsp_proxy_cleanup_command, (RTSP_PROXY_CLEANUP_MAX - 1), "iptables -I PREROUTING -t nat -p udp -s %d.%d.%d.%d --sport %d -d %d.%d.%d.%d --dport %d -j DNAT --to %d.%d.%d.%d:%d -m comment --comment \"rtsp_proxy; %lu\"",
		source_address[0],
		source_address[1],
		source_address[2],
		source_address[3],

		source_port,

		destination_address[0],
		destination_address[1],
		destination_address[2],
		destination_address[3],

		destination_port,

		client_address[0],
		client_address[1],
		client_address[2],
		client_address[3],

		destination_port,

		rtsp_proxy_system_uptime()
	);
	printf("RTSP PROXY: COMMAND: %s\n", rtsp_proxy_cleanup_command);
	system(rtsp_proxy_cleanup_command);
	rtsp_proxy_cleanup_command[10] = 'D';
}

static void rtsp_proxy_cleanup(){
	if(rtsp_proxy_cleanup_command[0] != 0){
		printf("RTSP PROXY: COMMAND: %s\n", rtsp_proxy_cleanup_command);
		system(rtsp_proxy_cleanup_command);
		rtsp_proxy_cleanup_command[0] = 0;
	}
	if(rtsp_proxy_server_socket >= 0)socket_tcp_close(rtsp_proxy_server_socket);
	rtsp_proxy_server_socket = -1;
}

static unsigned long int rtsp_proxy_system_uptime(){
	struct sysinfo si;
	sysinfo(&si);
	return((unsigned long int)si.uptime);
}

